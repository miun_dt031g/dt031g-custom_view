This branch is a continuation of the assignment4_handle_view_state branch. Following the pattern
described [here](https://guides.codepath.com/android/Creating-Custom-Listeners), the additions are:

1. RandomColorButton exposes a custom event called OnColorChange to notify listeners that the button has changed its color.
    * We define a public interface OnColorChangeListener with a method onColorChange(int).
    * We add a private field (named listener) of the type OnColorChangeListener to RandomColorButton.
    * We add a method setOnColorChangeListener(OnColorChangeListener) users of RandomColorButton can call to register them as a listener for changes in color.
    * In RandomColorButton::setColor(int) we call listener.onColorChange(newColor) to notify the listener that a new color is set.
2. MainActivity listens for color changes of the two RandomColorButton used in the activity.
    * A TextView has been added in res/layout/activity_main.xml to show the number of clicks each button have.
    * We add two private int fields to keep track of how many times each button has been clicked.
    * In onCreate we get references to the two buttons used in the activity.
    * We call setOnColorChangeListener on the two buttons, passing a OnColorChangeListener object in each call.
        * In the call back method onColorChange we increase number of clicks by one and then update the TextView with the new click number.
3. MainActivity saves/restores this data in i file in the app-specific local storage.    
    * We two methods, one for storing and one for reading the number of clicks.
        * To get the absolute path to this storage we call getFilesDir on a Context.
        * To get the filename to save/read we call SettingsActivity::getFilename.
    * In the activity's onResume callback method we call the read method to get the number of clicks.
        * But only if app setting for storing is set to true.
    * In the activity's onPause callback method we call the store method to save the number of clicks.
        * But only if app setting for storing is set to true.
4. A SettingsActivity is added.
    * In Android Studio we add an activity for settings by using menu item File --> New --> Activity - Settings Activity.
    * We remove the default preferences from res/xml/root_preference.xml.
    * We remove unused string values in strings.xml of the preferences removed.
    * We add an EditTextPreference for setting name of the file to use.
        * We add string values in strings.xml for:
            * the key of this preference
            * the title
            * the summary (which includes the value of this setting as per guidelines).
                * If you only need to show the actual value you can set app:useSimpleSummaryProvider="true" on this preference.
                * If you want to customize the summary you must do it in code (which i have done in SettingsFragment::onCreate).
                    * In strings.xml we use a format string for the summary. %1$s will be replaced in code with the name of the file.
        * We add a Vector Asset to use as an Image/Icon for this switch preference (save/store/add clipart).
        * We add a public static getFilename(Context) method in SettingsActivity which returns the settings value (the filename).
            * Using a SharedPreference and the key from strings.xml we can get to correct value.
            * This method any other class/object can call if they want to know the filename.
    * We add a Preference for resetting the number of clicks stored (we delete the file).
        * We add a string values in strings.xml for:
            * the key of this preference
            * the title
            * the summary
        * We add a Vector Asset to use as an Image/Icon for this preference (delete/remove/erase clipart).
        * We add a static deleteClicks(Context) method in SettingsActivity which deletes the text file.
            * Calls getFilename to get name of the file to delete. 
            * This method any other class/object can call if they want to reset the number of clicks.
        * In SettingsFragment (an inner class of SettingsActivity), we:
            * Add a preference click listener on the delete preference.
                * This is done in FragmentActivity::onCreate.
                * Call findPreference passing the key of the preference (defined in strings.xml).
            * Set a summaryProvider on the filename preference to automatic update the summary when the setting changes.
5. An options menu, available in the app bar, is added to MainActivity with which to user can open the settings activity.
    * In Android Studio we add an menu by right clicking on the res folder and selecting the menu option New --> Android Resource File.
        * Name the file main_menu.xml (for use in MainActivity).
        * Select Menu as resource type.
        * In the Menu design, add a Menu Item (drag-and-drop).
            * Give it an id.
            * As title always use 'Settings' for app preferences/settings (define in strings.xml)
            * A settings menu item should never be displayed as an action (set app:showAsAction="never").
    * In MainActivity we:
        * Override onCreateOptionsMenu and inflate our menu defined in main_menu.xml.
        * Override onOptionsItemSelected to handle selection of the settings menu item.
            * Start the SettingsActivity.
            
# Custom View Example #

Android offers a sophisticated and powerful componentized model for building your UI, based on the fundamental layout classes: View and ViewGroup. To start with, the platform includes a variety of prebuilt View and ViewGroup subclasses — called widgets and layouts, respectively — that you can use to construct your UI.

A partial list of available widgets includes Button, TextView, EditText, ListView, CheckBox, RadioButton, Gallery, Spinner, and more.

If none of the prebuilt widgets or layouts meets your needs, you can create your own View subclass, a Custom View or Custom component as I say in the course.

There are mainly three different approaches you can use to create a custom View.

1. Modifying an Existing View Type
    * If you only need to make small adjustments to an existing widget or layout, you can simply subclass the widget or layout and override its methods.
2. Fully Customized Components
    * Fully customized components can be used to create graphical components that appear however you wish. You usually start by extending the View class. You will then have to override onMeasure() and onDraw() if you want the component to show something.
3. Compound Controls
    * When creating a Compound Component (or Compound Control) you put together a reusable component that consists of a group of existing controls. n a nutshell, this brings together a number of more atomic controls (or views) into a logical group of items that can be treated as a single thing.
    
The focus in this course is creating Compound Controls.

## Compound Control ##

This example shows the way I recommend you to create a Compound Control. It consists of the following steps:

1. Create a new layout file (in xml) in which you design the look of your component. Preferably use androidx.constraintlayout.widget.ConstraintLayout as the root element in the layout.

2. Introduce your own (custom) attributes and parameters into the XML that can be pulled out and used by your class later.

3. Create a Java class that inherits from the same subclass of ViewGroup that the root element of your layout file uses.
 
3. In the new class, create matching constructors for the superclass, and pass their parameters through to the superclass constructor first.

4. Initialize your component, starting with inflating (load) the contents of the layout file.

5. After the layout has been inflated, you can get a reference to the UI components you defined in the XML layout file.

6. Get the value(s) of our custom attribute(s).

7. Create your own properties (instance variables) with accessors and modifiers that your class needs.

8. Override on... methods, like onTouchEvent, to perhaps act on user clicking on the component. Or set ...Listener, like OnClickListener, on views contained in your component. 

9. You can create listeners for events that your contained views might generate.

10. You can create listeners for events that your own properties might generate (for example a value of a variable is updated).

11. Implement saving and restoring UI state for your component (your own properties).