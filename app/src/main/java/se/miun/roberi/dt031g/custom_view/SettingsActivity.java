package se.miun.roberi.dt031g.custom_view;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.EditTextPreference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;

import java.io.File;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.settings, new SettingsFragment())
                    .commit();
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);

            // Handle click on delete preference
            findPreference(getString(R.string.pref_delete_key))
                    .setOnPreferenceClickListener(preference -> {
                        // Call method in SettingsActivity to reset number of clicks
                        return deleteClicks(getContext());
                    });

            // Set a custom summary provider on the preference for filename
            findPreference(getString(R.string.pref_filename_key))
                    .setSummaryProvider(preference -> {
                        // Get the value from the preference
                        String filename = ((EditTextPreference)preference).getText();

                        // R.string.pref_filename_summary contains the format syntax %1$s
                        // Using String.format %1$s will be replaced with the filename
                        // We returned the formatted string (which will be used by the
                        // SummaryProvider to set the summary of the preference
                        return String.format(getString(R.string.pref_filename_summary), filename);
                    });
        }
    }

    /**
     * Returns the name of the file in which to store number of clicks.
     *
     * @param context an application context
     * @return filename for storing number of clicks
     */
    public static String getFilename(Context context) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        return sharedPreferences.getString(context.getString(R.string.pref_filename_key),
                context.getString(R.string.pref_filename_default_value));
    }

    /**
     * Resets the number of clicks by deleting the file in which the clicks are stored.
     *
     * @param context an application context
     * @return true if and only if the file is successfully deleted; false otherwise
     */
    public static boolean deleteClicks(Context context) {
        // Get the absolute path to the directory on the filesystem where app-specific files are stored
        File localDir = context.getFilesDir();
        File clicksFile = new File(localDir, getFilename(context));
        return clicksFile.delete();
    }
}