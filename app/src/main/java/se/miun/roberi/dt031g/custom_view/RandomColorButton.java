package se.miun.roberi.dt031g.custom_view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TimeUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.google.android.material.internal.ParcelableSparseArray;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * A user interface component that displays a random color every time
 * the user clicks on the component. The hex value of the current color
 * is also shown.
 *
 * When creating a custom view using a layout file in xml, we need to
 * extend the same ViewGroup as the root element in the layout file. In
 * this example it is the ConstraintLayout class. Call the inflate method
 * to 'read' the xml layout file in which you defined the look.
 *
 * It is also important to create at least the two 'first' constructors that the
 * extending super class has. The ConstraintLayout has 4 constructors. This
 * is important because different ways to create a instance of this class
 * uses different constructor. After matching the constructors from the super
 * class, you can add your own if you want/need to.
 *
 * Instead of repeating the code of inflating the layout in every constructor,
 * add a private method that every constructor calls.
 *
 * In order for Android to save any View’s state, it is important that you assign an ID
 * for all the views you use (including custom views). Do this as you normally would do.
 *
 * In XML: android:id="@+id/button1"
 * In Java code: myRandomColorButton.setId(your_unique_id_as_an_int);
 *
 * if the instance of your View does not have an ID assigned.
 * myView.setId(R.id.desired_id);
 */
public class RandomColorButton extends ConstraintLayout {
    /**
     * Use this constant to indicate that the components color is not set.
     */
    private static final int UNDEFINED = -1;
    /**
     * The color (integer color) this component will display as its background color.
     */
    @ColorInt // Denotes that the annotated element represents a packed color int, AARRGGBB.
    private int color = UNDEFINED; // color not set

    /**
     * The View this custom component uses for setting the background color.
     */
    private View colorView;

    /**
     * The TextView this custom component uses for displaying the hex value
     * of the current color.
     */
    private TextView hexTextView;

    /**
     * Interface definition for a callback to be invoked when the color of a RandomColorButton is
     * changed. The color change happens when the button is clicked.
     */
    public interface OnColorChangeListener {
        void onColorChange(@ColorInt int newColor);
    }

    /**
     * Listener used to dispatch color change events to.
     */
    private RandomColorButton.OnColorChangeListener listener;

    /**
     * Register a callback to be invoked when the color changes (when this button is clicked).
     *
     * @param listener The callback that will run when the color changes
     * @see #setColor(int)
     */
    public void setOnColorChangeListener(OnColorChangeListener listener) {
        this.listener = listener;
    }

    /**
     * This constructor is often used when creating an instance in Java code.
     * For example, from an Activity:
     *
     * RandomColorButton myButton = new RandomColorButton(this);
     *
     * This constructor does’nt have access to XML attributes, you need to call
     * various setters manually. For example:
     *
     * myButton.setColor(Color.rgb(255, 0, 0));
     */
    public RandomColorButton(Context context) {
        super(context); // always call super first

        /*
         * Call our init method which inflates the layout this component uses.
         * We pass null as an argument since this constructor does not make use of
         * any XML attributes.
         */
        init(null);
    }

    /**
     * We can add more constructors if we have use for one. This is a 'custom'
     * constructor we can use from Java code if we want to set the color at
     * the same time we create a new instance. For example:
     *
     * RandomColorButton myButton = new RandomColorButton(this, Color.rgb(255, 0, 0));
     */
    public RandomColorButton(Context context, @ColorInt int color) {
        super(context); // always call super first

        /*
         * Call our init method which inflates the layout this component uses.
         * We pass null as an argument since this constructor does not make use of
         * any XML attributes.
         */
        init(null);
        setColor(color); // after the component is initialized, we can set the color
    }

    /**
     * This constructor is used when you create instances from an xml layout file.
     * For example, from activity_main.xml (where app:color is a custom attribute we
     * can use to set starting color for the button):
     *
     * <se.miun.roberi.dt031g.custom_view.RandomColorButton
     *   android:id="@+id/my_button"
     *   app:color="#FF0000"
     *   android:layout_width="match_parent"
     *   android:layout_height="match_parent" />
     *
     * Without this constructor, the layout inflater will crash. When a view is created
     * from an XML layout, all of the attributes in the XML tag are read from the resource
     * bundle and passed into the view's constructor as an AttributeSet.
     */
    public RandomColorButton(Context context, AttributeSet attrs) {
        super(context, attrs); // always call super first

        /*
         * Call our init method which inflates the layout this component uses.
         * We pass the attrs containing all XML attributes as an argument.
         */
        init(attrs);
    }

    /**
     * This constructor is meant to be called by superclasses to provide default style via
     * theme attribute and direct default style resource. The defStyleAttr parameter is the
     * reference to a style attribute defined in the theme.
     *
     * In this course we will not use this constructor. Just forward the context and attrs.
     */
    public RandomColorButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr); // always call super first

        /*
         * Call our init method which inflates the layout this component uses.
         * We pass the attrs containing all XML attributes as an argument.
         */
        init(attrs);
    }

    /**
     * This constructor is meant to be called by superclasses to provide default style via
     * theme attribute and direct default style resource. The defStyleAttr parameter is the
     * reference to a style attribute defined in the theme. The defStyleRes parameter is the
     * reference to the default style defined in styles.xml.
     *
     * In this course we will not use this constructor. Just forward the context and attrs.
     */
    public RandomColorButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes); // always call super first

        /*
         * Call our init method which inflates the layout this component uses.
         * We pass the attrs containing all XML attributes as an argument.
         */
        init(attrs);
    }

    /**
     * We use this private method to do all the initialization the component needs.
     * 1. The most important being inflating the layout this component uses.
     * 2. Get hold of the other UI components (defined in the XML layout file)
     * this component uses.
     * 3. Get the value(s) of our custom attribute(s).
     * 4. Update the colorView to show the color from attribute
     * (color set in constructor is handled in that constructor)
     * 5. Set a click listener on the colorView (click will randomize a new color)
     * 6. Call setSaveEnabled(true) to indicate that this view intends to save its state.
     */
    private void init(AttributeSet attrs) {
        // 1. Start by inflating the layout this component uses
        inflate(getContext(), R.layout.view_random_color_button, this);

        // 2. Now we can get a reference to the views this component uses
        colorView = findViewById(R.id.color_view);
        hexTextView = findViewById(R.id.color_hex);

        // 3a. Retrieve our custom attributes.
        TypedArray customAttributes = getContext().getTheme().obtainStyledAttributes(
                attrs, // The base set of attribute values. May be null.
                R.styleable.RandomColorButton, // Our custom attributes to be retrieved (in res/values/attrs.xml).
                0, 0); // 0 = do to not look for default values

        // 3b. Now we can get the value for the custom attributes we need. Since we set the format for our
        // attribute to color we call getColor. This returns the color value as an 'integer color'.
        int xmlColor = UNDEFINED;
        try {
            xmlColor = customAttributes.getColor( // we call getColor since our attribute format is color
                    R.styleable.RandomColorButton_startColor, // the attribute to get
                    UNDEFINED); // default value (integer color) if attribute is not defined
        } finally {
            // Note! TypedArray objects are a shared resource and must be recycled after use.
            customAttributes.recycle();
        }

        // 4. Update the colorView to show the color from attribute
        setColor(xmlColor);

        // 5. Set a click listener on the colorView
        colorView.setOnClickListener(view -> setRandomColor());

        // 6. Call setSaveEnabled(true) to indicate that this view intends to save its state.
        // Defaults to true, but it is good to know that this method is available.
        // If you set it to false, the onSaveInstanceState method will not be called.
        setSaveEnabled(true);
    }

    /**
     * Sets a new color for the component. If color is UNDEFINED (-1) the background will
     * be treated as white.
     */
    public void setColor(@ColorInt int color) {
        // We will use a 'Property Animation' to smoothly fade from the old color to the new.
        // Since it is a value (this.color) that is being animated, we use a ValueAnimator.
        ValueAnimator colorFadeAnimation = ValueAnimator.ofObject(
                new ArgbEvaluator(), // This evaluator can be used to perform type interpolation between integer values that represent ARGB colors.
                this.color, // The starting color
                color // The end color
        );

        // Set the new color for the component (after being used in the ValueAnimator)
        this.color = color;

        // When the new color i s set, call this button's OnColorChangeListener, if one is set.
        if (listener != null) {
            listener.onColorChange(this.color);
        }

        // Set the duration of the animation (in milliseconds)
        colorFadeAnimation.setDuration(200);

        // We need to add an update listener to update the background of the view
        colorFadeAnimation.addUpdateListener(animator -> {
            int animationColor = (int)animator.getAnimatedValue();
            colorView.setBackgroundColor(animationColor);

            // update the hex value for the new color
            // TODO: adjust text color of hexTextView for dark background colors
            String hexColor = String.format("#%06X", (0xFFFFFF & animationColor));
            hexTextView.setText(hexColor);
        });

        // If you do not want the hex value to be 'animated', add a listener to listen
        // for the animation to end. Then set the new hex color value (remove from above).

        // Start the animation
        colorFadeAnimation.start();
    }

    /**
     * Sets a new random color for the component.
     */
    public void setRandomColor() {
        /*
         * A color int in Android has 4 parts whit values between 0-255 (inclusive).
         * They are alpha, red, green and blue. This component does not support alpha
         * (will always use 255). We generate random number for red, green and blue.
         */
        Random random = new Random();
        int red = random.nextInt(256);
        int green = random.nextInt(256);
        int blue = random.nextInt(256);
        int randomColor = Color.rgb(red, green, blue);

        // Or use this one liner
        //int randomColor = 0xFF000000 | new Random().nextInt(0xFFFFFF);

        // Set the new color
        setColor(randomColor);
    }

    /**
    * The process of saving and restoring properties within a View involves two View
    * callbacks and an implementation of a Parcelable. Parcelable is an interface and
    * one type of Parcelable you might have noticed before is Bundle which the onCreate
    * method of an Activity takes as an argument.
    *
    * The Parcelable is responsible for serializing and deserializing properties of a
    * class. The View callbacks are responsible for passing the relevant information
    * into the Parcelable when saving state, and then appropriately applying data from
    * the Parcelable when restoring state.
    *
    * For saving/restoring properties of a custom view we must create a Parcelable that
    * holds the state (data) of our component. The easiest way to do this is to create a
    * inner static private class that extends BaseSaveState. This is a base class for
     * derived classes that want to save and restore their own state in onSaveInstanceState().
    */
    private static class SavedState extends BaseSavedState {
        // For each state/data the view will save/restore, create an instance variable.
        int color;

        /**
         * This constructor is used when we in onSaveInstanceState creates an instance
         * of this class. superState is the state from this custom views super class.
         */
        public SavedState(Parcelable superState) {
            super(superState);
        }

        /**
         * This constructor is for restoring the state of the view.
         */
        public SavedState(Parcel source) {
            super(source);

            /*
             * Use the Parcel provided to read back the state.
             * If you have more than one data to restore, the order in which you read
             * is important. Must be the same order as the data were saved.
             */
            color = source.readInt();
        }

        /**
         * This method is for writing the state of the view.
         */
        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);

            /*
             * Use the Parcel provided to write the state.
             * If you have more than one data to write, the order in which you write
             * is important. Must be the same order as the data is read.
             */
            out.writeInt(color);
        }

        /**
         * Classes implementing the Parcelable interface must also have a non-null
         * static field called CREATOR of a type that implements the Parcelable.Creator
         * interface. You always implement it like this:
         */
        public static final Parcelable.Creator<SavedState> CREATOR
                = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }

    /**
     * Now it is time to override the two callback methods from View. This method is used
     * when it is time to save the state of the view (this method is called automatically
     * by Android). We use the Parcelable returned from the super class, add our SaveState
     * class we just created and return the Parcelable to next class in the view hierarchy.
     */
    @Override
    protected Parcelable onSaveInstanceState() {
        // Obtain any state that our super class wants to save.
        Parcelable superState = super.onSaveInstanceState();

        // Wrap the super class's state with our own.
        SavedState randomColorButtonState = new SavedState(superState);
        randomColorButtonState.color = this.color;

        // Return our state along with our super class's state.
        return randomColorButtonState;
    }

    /**
     * When it is time for restoring the state, this method is called automatically.
     */
    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        // Start by casting the argument to our SavedState
        SavedState randomColorButtonState = (SavedState) state;

        // Get the previously saved super class state and pass it to super
        super.onRestoreInstanceState(randomColorButtonState.getSuperState());

        // Get and restore our saved state by calling the setColor method
       setColor(randomColorButtonState.color);
    }
}
